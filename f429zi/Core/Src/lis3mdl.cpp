#include "lis3mdl.hpp"
#include <cstdint>
#include <sstream>
#include <iostream>
#include <iomanip>

bool Id_Check(SPI_HandleTypeDef *hspi1)
{
	uint8_t mes = 1 << 7;
	mes |= WHO_AM_I_REG;
	uint8_t data = 0x00;
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(hspi1, &mes, 1, 100);
	HAL_SPI_Receive(hspi1, &data, 1, 100);
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
	if (data == WHO_AM_I)
		return true;
	else
		return false;
}

void Read_Reg(SPI_HandleTypeDef *hspi1, uint8_t reg, uint8_t &rData)
{
	uint8_t mes = 1 << 7;
	mes |= reg;
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(hspi1, &mes, 1, 100);
	HAL_SPI_Receive(hspi1, &rData, 1, 100);
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
}

void Config_Reg(SPI_HandleTypeDef *hspi1, uint8_t reg, uint8_t config)
{
	uint8_t mes = reg;
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(hspi1, &mes, 1, 100);
	HAL_SPI_Transmit(hspi1, &config, 1, 100);
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
}

void Init(SPI_HandleTypeDef *hspi1)
{
	uint8_t reg = 0x00;
	uint8_t config = 0x00;
	uint8_t buffer = 0x00;

	// Reg 1

	config = 0x00;
	buffer = 0x00;
	reg = 0x00;
	reg |= CTRL_REG1;
	config |= TEMP_ENABLE;
	config |= FAST_ODR_DISABLE;
	config |= DO_80Hz;
	config |= OM_HIGH;
	Read_Reg(hspi1, reg, buffer);
	if (buffer != config)
	{
		Config_Reg(hspi1, reg, config);
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
		}
	}

	// Reg 2

	config = 0x00;
	buffer = 0x00;
	reg = 0x00;
	reg |= CTRL_REG2;
	config |= FS_4Ga;
	Read_Reg(hspi1, reg, buffer);
	if (buffer != config)
	{
		Config_Reg(hspi1, reg, config);
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
		}
	}

	// Reg 4

	config = 0x00;
	buffer = 0x00;
	reg = 0x00;
	reg |= CTRL_REG4;
	config |= OMZ_HIGH;
	config |= LITTLE_Endian;
	Read_Reg(hspi1, reg, buffer);
	if (buffer != config)
	{
		Config_Reg(hspi1, reg, config);
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
		}

	}

	// Reg 5 BDU

	config = 0x00;
	buffer = 0x00;
	reg = 0x00;
	reg |= CTRL_REG5;
	config |= BDU_ENABLE;
	Read_Reg(hspi1, reg, buffer);
	if (buffer != config)
	{
		Config_Reg(hspi1, reg, config);
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
		}
	}

	// Reg 3 power on
	config = 0x00;
	buffer = 0x00;
	reg |= CTRL_REG3;
	config |= 0x00;
	Read_Reg(hspi1, reg, buffer);
	if (buffer != config)
	{
		Config_Reg(hspi1, reg, config);
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
		}
	}

}

void GetXYZ(SPI_HandleTypeDef *hspi1, int16_t *pData)

{
	uint8_t buffer[6];
	int i = 0;
	Read_Reg(hspi1, OUTX_L, buffer[0]);
	Read_Reg(hspi1, OUTX_H, buffer[1]);
	Read_Reg(hspi1, OUTY_L, buffer[2]);
	Read_Reg(hspi1, OUTY_H, buffer[3]);
	Read_Reg(hspi1, OUTZ_L, buffer[4]);
	Read_Reg(hspi1, OUTZ_H, buffer[5]);
	for (i = 0; i < 3; i++)

	{
		pData[i] = ((int16_t) ((uint16_t) buffer[2 * i + 1] << 8) + buffer[2 * i]);
	}
}

void XYZ_Read(SPI_HandleTypeDef *hspi1, UART_HandleTypeDef *huart3)
{
	int16_t data[3];
	GetXYZ(hspi1, data);

	std::ostringstream oss;
	oss << " X:" << std::setw(6) << std::setfill('0') << data[0] << " Y:"
			<< std::setw(6) << std::setfill('0') << data[1] << " Z:"
			<< std::setw(6) << std::setfill('0') << data[2] << std::endl;

	std::string str1 = oss.str();
	HAL_UART_Transmit(huart3, (uint8_t*) str1.c_str(), str1.length(), 1000);
	HAL_Delay(1000);
}
