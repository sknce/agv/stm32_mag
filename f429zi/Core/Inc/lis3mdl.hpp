#ifndef LIS3MDL_HPP
#define LIS3MDL_HPP
#include "config.hpp"
#include "main.h"

/*function to check connection with your module
@param hspi port spi*/
bool Id_Check(SPI_HandleTypeDef *hspi1);

/*function to change deaufults settings
@param hspi port spi
@param reg adress of the registery you want to change
@param config what you want to change*/
void Config_Reg(SPI_HandleTypeDef *hspi1, uint8_t reg, uint8_t config);

/* function read the given registery
@param hspi port spi
@param reg adress of the registery you want to read
@param data return the values of the registery */
void Read_Reg(SPI_HandleTypeDef *hspi1, uint8_t reg, uint8_t &rData);

void Init(SPI_HandleTypeDef *hspi1);

void GetXYZ(int16_t *pData);

void XYZ_Read(SPI_HandleTypeDef *hspi1, UART_HandleTypeDef *huart3);

#endif /* LIS3MDL_H_ */
