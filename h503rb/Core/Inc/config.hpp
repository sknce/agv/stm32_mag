/*
 * config.hpp
 *
 *  Created on: Oct 31, 2023
 *      Author: yeti2
 */

#ifndef INC_CONFIG_HPP_
#define INC_CONFIG_HPP_

/*///////////////////////////////////////////////
                 MODULE CHECK
*/
///////////////////////////////////////////////

#define WHO_AM_I_REG 0X0F
#define WHO_AM_I 0x3D

/*///////////////////////////////////////////////
                    REGISTERS
*/
///////////////////////////////////////////////

// |TEMP_EN| |OM1| |OM0| |DO2| |DO1| |DO0| |FAST_ODR| |ST|
#define CTRL_REG1 0X20

// |0| |FS1| |FS0| |0| |REBOOT| |SOFT_RST| |0| |0|
#define CTRL_REG2 0X21

// |0| |0| |LP| |0| |0| |SIM| |MD1| |MD0|
#define CTRL_REG3 0X22

// |0| |0| |0| |0| |OMZ1| |OMZ0| |BLE| |0|
#define CTRL_REG4 0X23

// |FAST_READ| |BDU| |0| |0| |0| |0| |0| |0|
#define CTRL_REG5 0X24

/*///////////////////////////////////////////////
                    REG 1
*/
///////////////////////////////////////////////

// Temperature  Default value: 0 (0: disabled; 1: enabled)
// ( REG_1 ) |TEMP_EN|

#define TEMP_DISABLE 0x00

#define TEMP_ENABLE 0x80

// FAST_ODR enables data rates higher than 80 Hz
// (REG_1) |FAST_ODR|

#define FAST_ODR_ENABLE 0x02

#define FAST_ODR_DISABLE 0x00

// Output data rate selection. Default value: 100
// ( REG_1 ) |DO2| |DO1| |DO0| If FAST_ODR_DISABLE

#define DO_0_625Hz 0x00

#define DO_1_25Hz 0x04

#define DO_2_5Hz 0x08

#define DO_5Hz 0x0C

#define DO_10Hz 0x10

#define DO_20Hz 0x14

#define DO_40Hz 0x18

#define DO_80Hz 0x1C

// Operating mode for X and Y axes Default value: 00
//( REG_1 ) |OM1| |OM0|    If FAST_ODR_ENABLE

#define OM_LOW_POWER 0x00 // 1000 Hz

#define OM_MEDIUM 0x20 // 560 Hz

#define OM_HIGH 0x40 // 300 Hz

#define OM_ULTRA_HIGH 0x60 // 155 Hz

/*///////////////////////////////////////////////
                    REG 2
*/
///////////////////////////////////////////////

// Full-scale configuration. Default value: 00
// ( REG_2 ) |FS1| |FS0|

#define FS_4Ga 0x00

#define FS_8Ga 0x20

#define FS_12Ga 0x40

#define FS_16Ga 0x60

// Reboot memory content. Default value: 0
// ( REG_2 ) |REBOOT|

#define REBOOT 0x80

// Configuration registers and user register reset function
//  ( REG_2 ) |SOFT_RST|

#define SOFT_RST 0x40

/*///////////////////////////////////////////////
                    REG 3
*/
///////////////////////////////////////////////

// ( REG_3 ) |LP|
/*Low-power mode configuration. Default value: 0
If this bit is ‘1’, DO[2:0] is set to 0.625 Hz and the system performs, for each
channel, the minimum number of averages. Once the bit is set to ‘0’, the magnetic
data rate is configured by the DO bits in CTRL_REG1 register */

#define LP_ENABLE 0x20

// SPI serial interface mode selection. Default value: 0
//(0: 4-wire interface; 1: 3-wire interface).
//  ( REG_3 ) |SIM|

#define SPI_3_WIRE 0x04

// Operating mode selection. Default value: 11
// ( REG_3 ) |MD1| |MD0|

#define MD_CONTINUOUS 0x00
/*Single-conversion mode (0x01) has to be used with sampling frequency from 0.625 Hz
to 80Hz. */
#define MD_SINGLE 0x01

#define MD_POWER_DOWN 0x02

#define MD_POWER_DOWN_AUTO 0x03

/*///////////////////////////////////////////////
                    REG 4
*/
///////////////////////////////////////////////

// Z-axis operative mode selection. Default value: 00.
//  ( REG_4 ) |OMZ1| |OMZ0|

#define OMZ_LOW_POWER 0x00

#define OMZ_MEDIUM 0x04

#define OMZ_HIGH 0x08

#define OMZ_ULTRA_HIGH 0x0C

// Big/Little Endian data selection. Default value: 0
//(0: data LSb at lower address; 1: data MSb at lower address)
//  ( REG_4 ) |BLE|

#define LITTLE_Endian 0x00

#define BIG_Endian 0x02



/*///////////////////////////////////////////////
                    REG 5
*/
///////////////////////////////////////////////

// FAST READ allows reading the high part of DATA OUT only in order to increase
// reading efficiency.
#define FAST_READ 0x80

/*Block data update for magnetic data. Default value: 0
(0: continuous update;
1: output registers not updated until MSb and LSb have been read)*/
// ( REG_4 ) |BDU|
#define BDU_DISABLE 0x00
#define BDU_ENABLE 0x40

/*///////////////////////////////////////////////
                    DATA READ
*/
///////////////////////////////////////////////

#define OUTX_L 0X28

#define OUTX_H 0X29

#define OUTY_L 0X2A

#define OUTY_H 0X2B

#define OUTZ_L 0X2C

#define OUTZ_H 0X2D

#define TEMP_OUT_L 0x2E
#define TEMP_OUT_H 0x2F


#endif /* INC_CONFIG_HPP_ */
