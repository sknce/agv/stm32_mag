/*
 * lis3mdl.hpp
 *
 *  Created on: Oct 31, 2023
 *      Author: yeti2
 */

#ifndef INC_LIS3MDL_HPP_
#define INC_LIS3MDL_HPP_
#include "config.hpp"
#include "main.h"
#include "stdbool.h"

/*function to check connection with your module
@param hspi port spi*/
bool Id_Check(SPI_HandleTypeDef *hspi1);

/*function to change deaufults settings
@param hspi port spi
@param reg adress of the registery you want to change
@param config what you want to change*/
void Config_Reg(SPI_HandleTypeDef *hspi1, uint8_t reg, uint8_t config);

/* function read the given registery
@param hspi port spi
@param reg adress of the registery you want to read
@param data return the values of the registery */
void Read_Reg(SPI_HandleTypeDef *hspi1, uint8_t reg, uint8_t &rData);

void Init(SPI_HandleTypeDef *hspi1);

void GetXYZ(int16_t *pData);

void XYZ_Read(SPI_HandleTypeDef *hspi1, UART_HandleTypeDef *huart3, float rData[]);




#endif /* INC_LIS3MDL_HPP_ */
