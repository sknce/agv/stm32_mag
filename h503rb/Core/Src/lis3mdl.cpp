/*
 * lis3mdl.cpp
 *
 *  Created on: Oct 31, 2023
 *      Author: yeti2
 */

#include <lis3mdl.hpp>
#include <string>


bool Id_Check(SPI_HandleTypeDef *hspi1)
{
	HAL_Delay(4);
	uint8_t mes = 1 << 7;
	mes |= WHO_AM_I_REG;
	uint8_t data = 0x00;
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(hspi1, &mes, 1, 100);
	HAL_SPI_Receive(hspi1, &data, 1, 100);
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
	if (data == WHO_AM_I)
		return true;
	else
		return false;
}

void Read_Reg(SPI_HandleTypeDef *hspi1, uint8_t reg, uint8_t &rData)
{
	uint8_t mes = 1 << 7;
	mes |= reg;
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(hspi1, &mes, 1, 100);
	HAL_SPI_Receive(hspi1, &rData, 1, 100);
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
}

void Config_Reg(SPI_HandleTypeDef *hspi1, uint8_t reg, uint8_t config)
{
	uint8_t mes = reg;
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(hspi1, &mes, 1, 100);
	HAL_SPI_Transmit(hspi1, &config, 1, 100);
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
}

void Init(SPI_HandleTypeDef *hspi1)
{
	if (Id_Check(hspi1))
	{
		// HAL_DELAY(4);
		uint8_t reg = 0x00;
		uint8_t config = 0x00;
		uint8_t buffer = 0x00;

		config = 0x00;
		buffer = 0x00;
		reg = 0x00;
		reg |= CTRL_REG1;
		config |= TEMP_ENABLE;
		config |= FAST_ODR_DISABLE;
		config |= DO_80Hz;
		config |= OM_HIGH;
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)
		{
			Config_Reg(hspi1, reg, config);
		}

		config = 0x00;
		buffer = 0x00;
		reg = 0x00;
		reg |= CTRL_REG2;
		config |= FS_4Ga;
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)
		{
			Config_Reg(hspi1, reg, config);
		}

		config = 0x00;
		buffer = 0x00;
		reg = 0x00;
		reg |= CTRL_REG4;
		config |= OMZ_HIGH;
		config |= LITTLE_Endian;
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)
		{
			Config_Reg(hspi1, reg, config);
		}

		config = 0x00;
		buffer = 0x00;
		reg = 0x00;
		reg |= CTRL_REG5;
		config |= BDU_ENABLE;
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)
		{
			Config_Reg(hspi1, reg, config);
		}

		config = 0x00;
		buffer = 0x00;
		reg |= CTRL_REG3;
		config |= 0x00;
		Read_Reg(hspi1, reg, buffer);
		if (buffer != config)

		{
			Config_Reg(hspi1, reg, config);
		}
	}
}

void GetXYZ(SPI_HandleTypeDef *hspi1, int16_t *pData)

{
	uint8_t buffer[6];
	int i = 0;
	Read_Reg(hspi1, OUTX_L, buffer[0]);
	Read_Reg(hspi1, OUTX_H, buffer[1]);
	Read_Reg(hspi1, OUTY_L, buffer[2]);
	Read_Reg(hspi1, OUTY_H, buffer[3]);
	Read_Reg(hspi1, OUTZ_L, buffer[4]);
	Read_Reg(hspi1, OUTZ_H, buffer[5]);
	for (i = 0; i < 3; i++)

	{
		pData[i] = ((int16_t) ((uint16_t) buffer[2 * i + 1] << 8) + buffer[2 * i]);
	}
}

void XYZ_Read(SPI_HandleTypeDef *hspi1, UART_HandleTypeDef *huart3, float rData[])
{
	int16_t data[3];
	GetXYZ(hspi1, data);
	rData[0] = (float) data[0] / 1711 * 100;
	rData[1] = (float) data[1] / 1711 * 100;
	rData[2] = (float) data[2] / 1711 * 100;
	HAL_Delay(1000);
}

/*void XYZ_Read(SPI_HandleTypeDef *hspi1, UART_HandleTypeDef *huart3)
 {
 int16_t data[3];
 GetXYZ(hspi1, data);

 char buffer[100]; // Assuming a sufficiently large buffer size for the string

 sprintf(buffer, "X:%06d Y:%06d Z:%06d\n", data[0], data[1], data[2]);

 std::string str1 = buffer;

 HAL_UART_Transmit(huart3, (uint8_t*)str1.c_str(), str1.length(), 1000);
 HAL_Delay(1000);
 }*/

// void calc(int16_t value, int8_t x, int8_t y, float gain)
//{
// float fvalue = value*gain;
// x = (int16_t)fvalue;
// y=(fvalue-x)*
//}
// void U2_to_string(int16_t value, char *buffer, int buffer_size) {
// 	int16_t gain;
// 	gain = 27368; // 4G
// 	// gain = 27368; // 8G
// 	// gain = 27372; // 12G
// 	// gain = 27376; // 16G
//     if (value <= gain) {
//         snprintf(buffer, buffer_size, "%d", value);
//     } else {
//         int16_t pos_value = (1 << 16) + value;
//         snprintf(buffer, buffer_size, "-%d", pos_value);
//     }
// }
